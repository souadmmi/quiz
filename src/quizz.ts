const questions = [
  {
    question: "What is the capital of Australia ?",
    answers: [
      { text: "Melbourne", correct: false },
      { text: "Sydney", correct: false },
      { text: "Canberra", correct: true },
      { text: "Hanoi", correct: false },
    ]
  },
  {
    question: "What is the capital of Burma ?",
    answers: [
      { text: "Kampala", correct: false },
      { text: "Naypyidaw", correct: true },
      { text: "Rangoon", correct: false },
      { text: "Mandalay", correct: false },
    ]

  },
  {
    question: "What is the capital of Brazil ?",
    answers: [
      { text: "Rio de Janeiro", correct: false },
      { text: "Kyiv", correct: false },
      { text: "Sao Paulo", correct: false },
      { text: "Brasilia", correct: true },
    ]
  },
  {
    question: "What is the capital of New Zealand ?",
    answers: [
      { text: "Auckland", correct: false },
      { text: "Christchurch", correct: false },
      { text: "Wellington", correct: true },
      { text: "Port-au-prince", correct: false },
    ]
  },
  {
    question: "What is the capital of Haiti ?",
    answers: [
      { text: "Kampala", correct: false },
      { text: "Havana", correct: false },
      { text: "Lima", correct: false },
      { text: "Port-au-prince", correct: true },
    ]
  },
  {
    question: "What is the capital of South Sudan ?",
    answers: [
      { text: "Dublin", correct: false },
      { text: "Tokyo", correct: false },
      { text: "Belgrade", correct: false },
      { text: "Juba", correct: true },
    ]
  },
  {
    question: "What is the capital of Ukraine ?",
    answers: [
      { text: "Managua", correct: false },
      { text: "Kyiv", correct: true },
      { text: "Sanaa", correct: false },
      { text: "Singapore", correct: false },
    ]
  },
  {
    question: "What is the capital of Mauritania ?",
    answers: [
      { text: "Nouakchott", correct: true },
      { text: "Port Louis", correct: false },
      { text: "Bishkek", correct: false },
      { text: "Jerusalem", correct: false },
    ]
  },
  {
    question: "What is the capital of Rwanda ?",

    answers: [
      { text: "Edinburgh", correct: false },
      { text: "Kigali", correct: true },
      { text: "Sarajevo", correct: false },
      { text: "Bujumbura", correct: false },
    ]
  },
  {
    question: "What is the capital of Madagascar ?",
    answers: [
      { text: "Muscat", correct: false },
      { text: "Antananarivo", correct: true },
      { text: "Paramaribo", correct: false },
      { text: "Tokyo", correct: false },
    ]
  }

];
//questions.splice(1);

//variable
const questionElement = document.querySelector<HTMLElement>("#question");
const answerButtons = document.querySelector<HTMLElement>("#answer-buttons");
const nextBtn = document.getElementById("next-btn");
const bouton = document.getElementsByClassName('btn');
const bout1 = document.getElementById("bout1");
const bout2 = document.getElementById("bout2");
const bout3 = document.getElementById("bout3");
const bout4 = document.getElementById("bout4");
const points = document.querySelector<HTMLElement>('.score');
const fin = document.querySelector<HTMLElement>("#fin");
const app = document.querySelector<HTMLElement>('.app');
const time = document.querySelector<HTMLElement>('.timeContainer')


let questionActuelleIndex = 0;
let score = 0;
let aRepondu = false;

//style score
points.textContent = score + "/" + questions.length;
points.style.textAlign = "center",
points.style.display = "flex";
points.style.justifyContent = "center";
points.style.alignItems = "center";
points.style.color = "#59230F";
points.style.fontWeight = "bold";



//afficher les questions reponses

if (questionElement && answerButtons) {
  questionElement.innerHTML = questions[questionActuelleIndex].question;
  for (let i = 0; i < bouton.length; i++) {
    bouton[i].innerHTML = questions[questionActuelleIndex].answers[i].text
  }
  points.textContent = questionActuelleIndex+1 + "/" + questions.length;

}

nextBtn.style.display = "none";

//faire en sorte que les boutons se remettent a leurs etats naturel apres avoir clicker sur next




nextBtn?.addEventListener('click', () => {
  questionActuelleIndex++
  if (questionActuelleIndex < questions.length) {
    defileQuestion()
    for (let i = 0; i < 4; i++) {
      let res = (eval("bout" + (i + 1)))
      let resC = document.getElementById(res.id)
      resC.style.backgroundColor = '#C4B197'
      resC.style.border = '2px solid #59230F'
      nextBtn.style.display = "none";

    }
  }else{    
    fini()
  }
})

function fini() {
  app.style.display = "none"
  fin.style.display = "block";
  fin.style.display = "flex";
  fin.style.justifyContent = "center";
  fin.style.alignItems = "center";
  fin.style.color = "#59230F"
  fin.style.fontSize = "3rem"
  fin.style.flexDirection= "column"
  fin.innerHTML = score + "/" + questions.length ;
  const paras = document.createElement('p');
  paras.style.textAlign="center"
  fin.append(paras)
  if (score == questions.length) {
    paras.innerHTML = "Congratulations, you got all the points !<br>✅"
  } if (score < questions.length && score >= 7) {
    paras.innerHTML =  "You were almost there! <br> 💪";
  } else if (score >= 5 && score < 7) {
    paras.innerHTML = "Next time you'll get it right! <br> 👌"
  } else if (score < 5 && score >=1) {
    paras.innerHTML = "You're so bad:/ <br> 😶";
  } else{
    paras.innerHTML = "OMG !:/<br>😱";
  }
}
//function du jeu 
function defileQuestion() {
  points.textContent = questionActuelleIndex+1 + "/" + questions.length;
  //afficher les questions
  if (questionElement && answerButtons) {
    questionElement.innerHTML = questions[questionActuelleIndex].question;
    for (let i = 0; i < bouton.length; i++) {
      bouton[i].innerHTML = questions[questionActuelleIndex].answers[i].text;
    }
  }
  
}

for (let i = 0; i < 4; i++) {
  bouton[i].addEventListener('click', () => {

    if (questions[questionActuelleIndex].answers[i].correct) {
      for (let i = 0; i < 4; i++) {
        if (questions[questionActuelleIndex].answers[i].correct) {
          let res = (eval("bout" + (i + 1)))
          let resC = document.getElementById(res.id)
          resC.style.backgroundColor = "rgba(102, 108, 73, 0.5)"
          resC.style.border = " 2px solid rgba(102, 108, 73, 1)"
          score++
          

          if (!aRepondu) {
            nextBtn.style.display = "block";
          }
        }
      }

    } else {
      if (!questions[questionActuelleIndex].answers[i].correct) {
        let res = (eval("bout" + (i + 1)))
        let resC = document.getElementById(res.id)
        resC.style.backgroundColor = "rgba(119, 16, 16, 0.5) "
        resC.style.border = " 2px solid #771010"
        if (!aRepondu) {
          nextBtn.style.display = "block";
        }

      }
      for (let i = 0; i < 4; i++) {
        if (questions[questionActuelleIndex].answers[i].correct) {
          let res = (eval("bout" + (i + 1)))
          let resC = document.getElementById(res.id)
          resC.style.backgroundColor = "rgba(102, 108, 73, 0.5)";
          resC.style.border = " 2px solid rgba(102, 108, 73, 1)";
          nextBtn.style.display = "block";

        }
      }

    }


  })
  aRepondu = false

}



//Time 
/*  let i= 0
  let jsp = 11
  const t = setInterval(()=>{
      i++
      time.textContent=(jsp - i)
      if(i>11){
          time.textContent='Cest fini!!!'; 
      } 
  },1000)

*/










